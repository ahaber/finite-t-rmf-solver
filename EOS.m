(* ::Package:: *)

(* ::Input::Initialization:: *)
BeginPackage["EOS`"];
Needs["RMFsolver`"];
Needs["Constants`"];
(*public functions*)
{slope$sym,RMF$ebind$PNM$fit,RMF$ebind$SYM$fit,analyze$RMF,analyze$fit,attach$crust$tab,attach$crust,offset$pressure,EPNMfunc,PPNMfunc,polyspace,M$r$curve,find$mmax,analyze$RMF2,attach$crust$tab2}

Begin["`Private`"]
(*module to compute slopre of symmetry energy from tables {nb[MeV^4],Energy of SNM in MeV^4} and {nb[MeV^4],Energy of PNM in MeV^4}*)
slope$sym[esymtab_,epnmtab_,nsat_]:=Module[{esymfun,epnmfun,nb,symen,slope,pl},
esymfun[nb_]=Interpolation[esymtab][nb];
epnmfun[nb_]=Interpolation[epnmtab][nb];
symen[nb_]:=esymfun[nb]-epnmfun[nb];
slope=3*nsat*D[symen[x],x]/.x->nsat;
Return[slope]
]



(*analyze any RMF of the form suitable as input for RMFsolve*)
analyze$RMF[para_]:=Module[{nbrestabPNM,nbrestabSNM,fitpara,presstabSNM,presstabPNM,EbindtabPNM,EbindtabSNM,i,pl1,pl2,nucdens,nbrestabSNMn0,enertab,incomp$sat,nbsat,x,u,Esat,esym,slopesym,esymfun},

nbrestabPNM=RMFsolve$nb$PNM[0.5*NuclearDensity$nucleons$MeV3,2*NuclearDensity$nucleons$MeV3,30,0,para,30,20,-3,990,False];
nbrestabSNM=RMFsolve$nb$SYM[0.5*NuclearDensity$nucleons$MeV3,2*NuclearDensity$nucleons$MeV3,30,0,para,30,20,990,False];
EbindtabPNM=Table[{nbrestabPNM[[i,1]],binding$energy$RMF[nbrestabPNM[[i,2]]]},{i,1,Length[nbrestabPNM]}];
EbindtabSNM=Table[{nbrestabSNM[[i,1]],binding$energy$RMF[nbrestabSNM[[i,2]]]},{i,1,Length[nbrestabSNM]}];
nbsat=u/.FindRoot[(D[Interpolation[EbindtabSNM][x],x]/.x->u)==0,{u,0.98*NuclearDensity$nucleons$MeV3}];
Esat=Interpolation[EbindtabSNM][nbsat];
esymfun[x_]=Interpolation[EbindtabPNM][x]-Interpolation[EbindtabSNM][x];
nbrestabSNMn0=RMFsolve$nb$SYM[0.999*nbsat,1.001*nbsat,2,0,para,30,20,990,False];
enertab=Table[{nbrestabSNMn0[[i,1]],edens$RMF[nbrestabSNMn0[[i,2]]]},{i,1,Length[nbrestabSNMn0]}];
incomp$sat=9*nbsat*(Chop[enertab[[3,2]]-2enertab[[2,2]]+enertab[[1,2]]])/(Chop[enertab[[1,1]]-enertab[[2,1]]])^2;
slopesym=3*nbsat*D[esymfun[x],x]/.x->nbsat;
Return[{nbsat/MeV$fm^3,Esat,incomp$sat,esymfun[nbsat],slopesym}]
];

(*input: EOS table for RMF, for crust, nb(mu) table of RMF (can be rewritten s.t. this table is included in eos$RMF), offset pressure+bag constant)*)
offset$pressure[eos$RMF_,eos$crust_,nb$of$muB$RMF_,Poff_]:=Module[{k,mu$of$P$RMF,mu$of$P$RMFfun,pcross,p,nbcr,mu$of$P$RMFoff,mu$of$P$crfun,nb$of$mu$crfun},
mu$of$P$RMF=eos$RMF[[All,{1,3}]];
mu$of$P$RMFoff={#1+Poff,#2}&@@@mu$of$P$RMF;
mu$of$P$RMFfun=Interpolation[mu$of$P$RMFoff];
mu$of$P$crfun=Interpolation[eos$crust[[All,{2,3}]]];
nb$of$mu$crfun=Interpolation[eos$crust[[All,{3,1}]]];
pcross=p/.FindRoot[{mu$of$P$crfun[p]-mu$of$P$RMFfun[p]==0},{p,100000,1,10000000}];
nbcr=nb$of$mu$crfun[mu$of$P$RMFfun[pcross]]/MeV$fm^3/0.16;
Return[{pcross,mu$of$P$crfun[pcross],mu$of$P$RMFfun[pcross],nbcr}]
];

(*module that combines crust+core at a given transitoin crust baryon denisity (first order phase transition) where mu_B and P are smoothly connected at the phase transitoin and n_B is a monotonic function of mu_B*)
attach$crust[nbtrans_,eos$RMF_,eos$crust_,nb$of$muB$RMF_]:=Module[{Poff,po,epsofP$RMF,eps$of$P,p,epsofP$RMF$fun,pcross,mu,nb$of$mu,e$of$pcrfun},
po=Poff/.FindRoot[offset$pressure[eos$RMF,eos$crust,nb$of$muB$RMF,Poff][[4]]==nbtrans,{Poff,-0.5*10^6},Evaluated->False][[1]];
epsofP$RMF={#1+po,#2-po}&@@@eos$RMF[[All,{1,2}]];
epsofP$RMF$fun=Interpolation[epsofP$RMF,InterpolationOrder->1];
pcross=offset$pressure[eos$RMF,eos$crust,nb$of$muB$RMF,po];
e$of$pcrfun[p_]=Interpolation[eos$crust[[All,{2,5}]],InterpolationOrder->1][p];
eps$of$P[p_]=If[p<=pcross[[1]],e$of$pcrfun[p],epsofP$RMF$fun[p]];
Return[{eps$of$P,epsofP$RMF[[-1,1]],pcross,po}]; 
];(*output is interpolating function, range of validity i.e. maxiumum pressure, transition pressure in MeV^4 and bag constant in MeV^4*)

(*some module as above, but output is a table *)
attach$crust$tab[nbtrans_,eos$RMF_,eos$crust_,nb$of$muB$RMF_]:=Module[{Poff,bagconstant,epsofP$RMF,eps$of$P,p,epsofP$RMF$fun,pcross,mu,nb$of$mu,e$of$pcrfun,epsofP$cr,l1,l2,eps$of$P$joined},
bagconstant=Poff/.FindRoot[offset$pressure[eos$RMF,eos$crust,nb$of$muB$RMF,Poff][[4]]==nbtrans,{Poff,-0.5*10^6},Evaluated->False][[1]];
epsofP$RMF={#1+bagconstant,#2-bagconstant}&@@@eos$RMF[[All,{1,2}]];
epsofP$cr=eos$crust[[All,{2,5}]];
pcross=offset$pressure[eos$RMF,eos$crust,nb$of$muB$RMF,bagconstant];
l1=Select[epsofP$cr,#[[1]]<pcross[[1]]&];
l2=Select[epsofP$RMF,#[[1]]>pcross[[1]]&];
eps$of$P$joined=Join[l1,l2];

Return[{eps$of$P$joined,epsofP$RMF[[-1,1]],pcross,bagconstant}];];
(*this function gives a more complete outpout including a combine table of mu_B, nB,P and e , old function is kept for compatibility reasins*)

attach$crust$tab2[nbtrans_,eos$RMF_,eos$crust_,nb$of$muB$RMF_]:=Module[{Poff,bagconstant,epsofP$RMF,eps$of$P,p,epsofP$RMF$fun,pcross,mu,nb$of$mu,e$of$pcrfun,epsofP$cr,l1,l2,eps$of$P$joined,crusttab,rmftab,nuctab},
bagconstant=Poff/.FindRoot[offset$pressure[eos$RMF,eos$crust,nb$of$muB$RMF,Poff][[4]]==nbtrans,{Poff,-0.5*10^6},Evaluated->False][[1]];
epsofP$RMF={#1+bagconstant,#2-bagconstant}&@@@eos$RMF[[All,{1,2}]];
epsofP$cr=eos$crust[[All,{2,5}]];
pcross=offset$pressure[eos$RMF,eos$crust,nb$of$muB$RMF,bagconstant];
crusttab={#2,#5,#3,#1}&@@@Select[eos$crust,#[[2]]<pcross[[1]]&];

rmftab=Select[Table[{epsofP$RMF[[i,1]],epsofP$RMF[[i,2]],nb$of$muB$RMF[[i,1]],nb$of$muB$RMF[[i,2]]},{i,1,Length[nb$of$muB$RMF]}],#[[1]]>pcross[[1]]&];
l1=Select[epsofP$cr,#[[1]]<pcross[[1]]&];
l2=Select[epsofP$RMF,#[[1]]>pcross[[1]]&];
eps$of$P$joined=Join[l1,l2];
nuctab=Join[crusttab,rmftab];
Return[{eps$of$P$joined,epsofP$RMF[[-1,1]],pcross,bagconstant,rmftab,nuctab}];];


(*function that finds maximum of an M-R curve, as well as R(1.4Msun), R(Mmax) and checks whether curve has reached a maximum (topped=1) or not (topped=0*)
(*input Table[{R[km],M/Msun}]*)

find$mmax[mrtab_]:=Module[{Rmmax,R,x,rad,mmax,R14,topped,mrfun,mrtab2,rmtab},
mrtab2=Select[mrtab,#[[2]]>=1.7&];
rmtab=mrtab[[All,{2,1}]];
mrfun[x_]=Interpolation[mrtab2,InterpolationOrder->3][x];
Rmmax=x/.FindRoot[(D[mrfun[R],R]/.R->x)==0,{x,11}];
mmax=mrfun[Rmmax];
R14=Interpolation[rmtab][1.4];
If[Max[mrtab[[All,2]]]==mrtab[[-1,2]],topped=0,topped=1];
Return[{mmax,Rmmax,R14,topped}];
];



End[];
EndPackage[];
