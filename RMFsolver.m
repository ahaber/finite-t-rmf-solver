(* ::Package:: *)

(* ::Title:: *)
(*RMF Solver Package*)


(* ::Input::Initialization:: *)
(*Solver for general RMF for npe-matter in beta equilibrium and charge neutrality, pure neutron matter (PNM), or symmetric nuclear matter (SYM), T=0 and finite T included,  written by Alexander Haber, updated version to include finite T correctly, May 2024*)

BeginPackage["RMFsolver`"];
(*public functions*)
{RMFsolve,RMFsolve$nu,proton$fraction$RMF,RMFpressure,electron$pressure,n$electron,n$neutrino,nucleon$pressure,RMFsolveSYM,RMFpressureSYM,edens$RMF,pressure$RMF,entropy$RMF,binding$energy$RMF,pressure$RMF$PNM,RMFpressurePNM,RMFsolvePNM,RMFbindingPNM,RMFsolve$nb,baryon$density,RMFbindingSYM,RMFsolve$nb$PNM,RMFsolve$nb$SYM,create$EOS,DeltaMU,dU$threshold,freeener$RMF,RMFsolve$mu,RMFsolve$nonequil,RMFsolve$nonequil$xpconst,RMFsolve$xp,neutrino$test,neutrino$pressure,neutrino$entropy,neutrino$edens,RMFsolve$nu$brent, pressure$hesse,RMFsolveSYM$renorm,ds$dT,d2s$dT2,RMFsolve$muB$muQ,analyze$dPds}

Begin["`Private`"]
(*Lagrangian of SFH0 RMF (see compose for couplings) https://compose.obspm.fr/eos/34, Lagrangian in nucl-th/0410066,*)
fswr[\[Sigma]_,w0_]:=(b1*w0^2+b2*w0^4+b3*w0^6+a1*\[Sigma]+a2*\[Sigma]^2+a3*\[Sigma]^3+a4*\[Sigma]^4+a5*\[Sigma]^5+a6*\[Sigma]^6); (*crosscoupling terms between rho/omega and rho/sigma*)
U$sigma[s_]:=1/2*ms^2*s^2+b*Mn/3(gsn*s)^3+c/4(gsn*s)^4; (*sigma potential*)
(*meson lagrangian*)
Lmes[\[Sigma]_,w0_,r03_]:= -U$sigma[\[Sigma]]+1/2*mw^2*w0^2+zet/24*gw[[1]]^4*w0^4+1/2*mr^2*r03^2+xi/24*gr[[1]]^4*r03^4+gr[[1]]^2*fswr[\[Sigma],w0]r03^2;
(*renormalization terms*)
Lmes$renorm[\[Sigma]_,w0_,r03_] :=Lmes[\[Sigma],w0,r03]+1/(4\[Pi]^2)*(Mn^3*gs[[1]]*\[Sigma]-7/2*Mn^2*(gs[[1]]*\[Sigma])^2+13/3*Mn*(gs[[1]]*\[Sigma])^3-25/12(gs[[1]]*\[Sigma])^4+(mB[[1]]-gs[[1]]*\[Sigma])^4*Log[(mB[[1]]-gs[[1]]*\[Sigma])/mB[[1]]]); (*this probably is only correct if Mn=mB[[1]]=mB[[2]]!!*)
(*baryon density for interacting baryons*)
nB[T_?NumericQ,\[Mu]B_?NumericQ,BI_?NumericQ,\[Sigma]_?NumericQ,w0_?NumericQ,r03_?NumericQ]:=If[T>=0.1,1/\[Pi]^2*NIntegrate[k^2/(1+Exp[(Sqrt[k^2+(mB[[BI]]-gs[[BI]]*\[Sigma])^2]-(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03))/T])-k^2/(1+Exp[(Sqrt[k^2+(mB[[BI]]-gs[[BI]]*\[Sigma])^2]+(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*bar*r03))/T]),{k,0,upB}],If[(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03)>(mB[[BI]]-gs[[BI]]*\[Sigma]),Sqrt[(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03)^2-(mB[[BI]]-gs[[BI]]*\[Sigma])^2]^3/(3\[Pi]^2),0]];

(*baryon density for interacting baryons - public function that sets parameters as well*)
baryon$density[T_?NumericQ,\[Mu]B_?NumericQ,BI_?NumericQ,\[Sigma]_?NumericQ,w0_?NumericQ,r03_?NumericQ,para_]:=Module[{dens},
set$couplings[para];
dens=nB[T,\[Mu]B,BI,\[Sigma],w0,r03];
Return[dens]
];

(*baryon scalar density, source term for sigma meson*)
ns[T_?NumericQ,\[Mu]B_?NumericQ,BI_?NumericQ,\[Sigma]_?NumericQ,w0_?NumericQ,r03_?NumericQ]:=If[T>=.1,1/Pi^2(NIntegrate[(mB[[BI]]-gs[[BI]]*\[Sigma])/Sqrt[(k^2+(mB[[BI]]-gs[[BI]]*\[Sigma])^2)]k^2*(1/(1+Exp[(Sqrt[k^2+(mB[[BI]]-gs[[BI]]*\[Sigma])^2]-(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03))/T])+1/(1+Exp[(Sqrt[k^2+(mB[[BI]]-gs[[BI]]*\[Sigma])^2]+(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*bar*I3[[BI]]*r03))/T])),{k,0,upB}]),(mB[[BI]]-gs[[BI]]*\[Sigma])/(2\[Pi]^2)(Sqrt[(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03)^2-(mB[[BI]]-gs[[BI]]*\[Sigma])^2]*((\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03))-(mB[[BI]]-gs[[BI]]*\[Sigma])^2Log[(Sqrt[(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03)^2-(mB[[BI]]-gs[[BI]]*\[Sigma])^2]+(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03))/(mB[[BI]]-gs[[BI]]*\[Sigma])])];


(*Fermi momentum for baryons in RMF, needs set$couplings*)
kf$bar[\[Sigma]_,w0_,r03_,\[Mu]B_,BI_]:=If[(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03)>(mB[[BI]]-gs[[BI]]*\[Sigma])&&(mB[[BI]]-gs[[BI]]*\[Sigma])>0,Sqrt[(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03)^2-(mB[[BI]]-gs[[BI]]*\[Sigma])^2],0];

(*E_F^* for baryons = Sqrt[kf^2+m*^2] (no vector mean fields), needs set$couplings*)
Ef$bar[\[Sigma]_,w0_,r03_,\[Mu]B_,BI_]:=Sqrt[kf$bar[\[Sigma],w0,r03,\[Mu]B,BI]^2+(mB[[BI]]-gs[[BI]]*\[Sigma])^2];


(*fermi dirac distribution*)
fd[E_,mu_,T_]:=1/(1+Exp[(E-mu)/T]);
(*Fermi Dirac distribution for Baryons*)
fd$bar[k_,\[Sigma]_,w0_,r03_,\[Mu]B_,BI_,T_]:=1/(1+Exp[(Sqrt[k^2+(mB[[BI]]-gs[[BI]]*\[Sigma])^2]-(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03))/T]);
(*fermion pressure for free fermion, needs set$couplings*)(*???*)



(* ::Subtitle:: *)
(*Electron Contributions*)


(* ::Input::Initialization:: *)
(*electrons=free fermion density, needs setcouplings*)
n$electron[T_?NumericQ,\[Mu]Q_?NumericQ]:=If[T>= .1,1/\[Pi]^2*NIntegrate[k^2/(1+Exp[(Sqrt[k^2+me^2]-\[Mu]Q)/T]),{k,0,upB}]-1/\[Pi]^2*NIntegrate[k^2/(1+Exp[(Sqrt[k^2+me^2]+\[Mu]Q)/T]),{k,0,upB}],If[\[Mu]Q>me,1/(3\[Pi]^2)*Sqrt[\[Mu]Q^2-me^2]^3,0]];
kF$electron[muQ_?NumericQ]:=CubeRoot[3*\[Pi]^2*n$electron[0,muQ]];
electron$pressure[T_,muq_]:=If[T==0,1/(8\[Pi]^2)(2/3*muq^4),NIntegrate[2*T*k^2*4\[Pi]/(2\[Pi])^3*(Log[1+Exp[-(Sqrt[k^2+me^2]-muq)/T]]+Log[1+Exp[-(Sqrt[k^2+me^2]+muq)/T]]),{k,0,upB}]];


(* ::Subtitle:: *)
(*Neutrino Contributions*)


(* ::Input::Initialization:: *)
n$neutrino[T_?NumericQ,\[Mu]nu_?NumericQ,up_:4000]:=If[T>= .1,1/(2\[Pi]^2)*NIntegrate[k^2fd[k,\[Mu]nu,T],{k,0,up}]-1/(2\[Pi]^2)*NIntegrate[k^2fd[k,-\[Mu]nu,T],{k,0,up}],1/(6\[Pi]^2)*Sqrt[\[Mu]nu^2]^3];
neutrino$pressure[T_?NumericQ,munu_?NumericQ,up_:4000]:=If[T==0,1/(8\[Pi]^2)*(1/3*munu^4),NIntegrate[T*k^2*4\[Pi]/(2\[Pi])^3*(Log[1+Exp[-(k-munu)/T]]+Log[1+Exp[-(k+munu)/T]]),{k,0,up}]];
neutrino$edens[T_?NumericQ,munu_?NumericQ,up_:4000]:=If[T<=0.1,+munu^4/(8\[Pi]^2),Sum[+4\[Pi]/(2\[Pi])^3*NIntegrate[k^3/(1+Exp[(k-sgn*munu)/T]),{k,0,up}],{sgn,-1,1,2}]];
neutrino$entropy[T_?NumericQ,munu_?NumericQ,up_:4000]:=If[T>0,Sum[-1/(2\[Pi])^3*4\[Pi]*NIntegrate[k^2*(fd[k,sgn*munu,-T]*Log[fd[k,sgn*munu,-T]]+fd[k,sgn*munu,T]*Log[fd[k,sgn*munu,T]]),{k,0,4000}],{sgn,-1,+1,2}],0]; (*constant upper bound of 4000MeV seems to work best for neutrinos, T dependent bounds tend to fail*);
(*this functions tests the thermodynamic relation P=-eps+Ts+mu*n for neutrinos*)
neutrino$test[T_,munu_,up_:4000]:=Module[{edens$neutrino,pressure$neutrino,entropy$neutrino,kfnu},
kfnu=(6*\[Pi]^2*n$neutrino[T,munu,up])^(1/3);
(*Print["kfnu=",kfnu];
Print["bound=",kfnu+boundmult*T];*)
edens$neutrino=neutrino$edens[T,munu,up]
(*Print["E=",edens$neutrino]*);
pressure$neutrino=neutrino$pressure[T,munu,up];
(*Print["P=",pressure$neutrino];*)
entropy$neutrino=neutrino$entropy[T,munu,up]; (*constant upper bound of 4000MeV seems to work best for neutrinos, T dependent bounds tend to fail*)
(*Print["S=",entropy$neutrino];*)
Return[{pressure$neutrino-(-edens$neutrino+munu*n$neutrino[T,munu,up]+T*entropy$neutrino)}]
];



(* ::Subtitle:: *)
(*Photon Contributions*)


(* ::Input::Initialization:: *)
photon$pressure[T_]:=T^4*\[Pi]^2/45//N;
photon$entropy[T_]:=T^3*4*\[Pi]^2/45//N;
photon$energy[T_]:=T^4*\[Pi]^2/15//N;
photon$freeener[T_]:=-\[Pi]^2/45*T^4//N;


(* ::Subtitle:: *)
(*Nucleon Contributions*)


(* ::Input::Initialization:: *)
(*Module to set couplings, me and upper bound for NIntegrate FIXED!*)
(*para={{mn,mp},{m$sigma,m$omega,m$rho},{I3n,I3p},{gsn,gsp},{gwn,gwp},{grn,grp},{b,c,Mn$scale},{omega4$coupling},{rho4$coupling},{b1,b2,b3,a1,a2,a3,a4,a5,a6},nsat,bar};*)
set$couplings[para_]:=Module[{},
ClearAll[ms,mw,mr,b,c,Mn,mB,gsn,grn,gwn,gsp,gwp,grp,gs,gw,gr,I3,I3n,I3p,zet,xi,b1,b2,b3,a1,a2,a3,a4,a5,a6,me,upB];
ms=para[[2,1]];
mw=para[[2,2]];
mr=para[[2,3]];
b=para[[7,1]];
c=para[[7,2]];
Mn=para[[7,3]];
mB=para[[1]];
gsn=para[[4,1]];
gs=para[[4]];
gw=para[[5]];
gr=para[[6]];
I3=para[[3]];
zet=para[[8,1]];
xi=para[[9,1]];
bar=-1;
b1=para[[10,1]];b2=para[[10,2]];b3=para[[10,3]];a1=para[[10,4]];a2=para[[10,5]];a3=para[[10,6]];a4=para[[10,7]];a5=para[[10,8]];a6=para[[10,9]];me=0.511;upB=5000;
P$offset=If[NumericQ[para[[13]]],para[[13]],0];
]
(*fermion pressure for baryons, needs set$couplings*)
nucleon$pressure[T_,mu_,sig_,w0_,r03_,BI_]:=If[T<=0.1,1/(8\[Pi]^2)*((2/3kf$bar[sig,w0,r03,mu,BI]^3-(mB[[BI]]-gs[[BI]]*sig)^2kf$bar[sig,w0,r03,mu,BI])Ef$bar[sig,w0,r03,mu,BI]+(mB[[BI]]-gs[[BI]]*sig)^4*(Log[(kf$bar[sig,w0,r03,mu,BI]+Ef$bar[sig,w0,r03,mu,BI])/((mB[[BI]]-gs[[BI]]*sig))])),2*T*NIntegrate[4\[Pi]/(2\[Pi])^3k^2(Log[1+Exp[-(Sqrt[k^2+(mB[[BI]]-gs[[BI]]*sig)^2]-(mu-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03))/T]]+Log[1+Exp[-(Sqrt[k^2+(mB[[BI]]-gs[[BI]]*sig)^2]+(mu-gw[[BI]]*w0-bar*gr[[BI]]*I3[[BI]]*r03))/T]]),{k,0,upB}]];
(*energy density of one specific baryon species in RMF , needs set$couplings
nucleon$edens[T_,mu_,sig_,w0_,r03_,BI_]:=nucleon$pressure[T,mu,sig,w0,r03,BI]-mu*nB[T,mu,BI,sig,w0,r03];*)


(* ::Input::Initialization:: *)
pressure$hesse[T_,mu_,sig_,w0_,r03_,para_]:=Module[{c11,c12,c22,s,w,r,hesse},
set$couplings[para];
c11=Sum[D[nucleon$pressure[T,mu,s,w,r,BI],s,s]/.{s->sig,w->w0,r->r03},{BI,1,2,1}]+D[Lmes[s,w,r],s,s]/.{s->sig,w->w0,r->r03};
c22=Sum[D[nucleon$pressure[T,mu,s,w,r,BI],w,w]/.{s->sig,w->w0,r->r03},{BI,1,2,1}]+D[Lmes[s,w,r],w,w]/.{s->sig,w->w0,r->r03};
c12=Sum[D[nucleon$pressure[T,mu,s,w,r,BI],s,w]/.{s->sig,w->w0,r->r03},{BI,1,2,1}]+D[Lmes[s,w,r],s,w]/.{s->sig,w->w0,r->r03};
hesse={{c11,c12},{c12,c22}};
Return[{hesse,Eigenvalues[hesse]}]
]


(* ::Chapter:: *)
(*RMF Equations Solvers*)


(* ::Input::Initialization:: *)
(*RMFsolver at given baryon chemical potential, automatically enforces beta equilibrium and charge neutrality*)
RMFsolve$mu[mub_,Trmf_?NumericQ,para_,\[Sigma]s_?NumericQ,w0s_?NumericQ,r03s_?NumericQ,\[Mu]es_?NumericQ,verb_?BooleanQ]:=Module[{reslist,Eq1,Eq2,Eq3,Eq4,Eq5,s,w,rr,\[Sigma],w0,r03,\[Mu]e},
set$couplings[para];
If[verb==True,If[Trmf<= .1,Print["Temperature is either 0 or T< 1MeV, T too small for finite T numerical integration, T=0 RMF solver is used"]]];

Eq1[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]e_]:=-(gs[[1]]*ns[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gs[[2]]*ns[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03])-D[Lmes[s,w0,r03],s]/.{s->\[Sigma]};
Eq2[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]e_]:=-(gw[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gw[[2]]*nB[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w,r03],w]/.{w->w0};
Eq3[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]e_]:=-(gr[[1]]*I3[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gr[[2]]*I3[[2]]*nB[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w0,rr],rr]/.{rr->r03};
Eq5[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]e_]:=n$electron[Trmf,\[Mu]e]-nB[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03];

reslist=FindRoot[{Eq1[\[Sigma],w0,r03,mub,\[Mu]e]==0,Eq2[\[Sigma],w0,r03,mub,\[Mu]e]==0,Eq3[\[Sigma],w0,r03,mub,\[Mu]e]==0,Eq5[\[Sigma],w0,r03,mub,\[Mu]e]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{r03,Re[r03s]},{\[Mu]e,Re[\[Mu]es]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,r03/.reslist,mub,(mub-\[Mu]e)/.reslist,(\[Mu]e)/.reslist,0},{(Eq1[\[Sigma],w0,r03,mub,\[Mu]e]/.reslist),(Eq2[\[Sigma],w0,r03,mub,\[Mu]e]/.reslist),Eq3[\[Sigma],w0,r03,mub,\[Mu]e]/.reslist,Eq5[\[Sigma],w0,r03,mub,\[Mu]e]/.reslist},Trmf,nB[Trmf,mub,1,\[Sigma]/.reslist,w0/.reslist,r03/.reslist]+nB[Trmf,(mub-\[Mu]e)/.reslist,2,\[Sigma]/.reslist,w0/.reslist,r03/.reslist]/.reslist,para};
chk=out[[2]];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]
]
analyze$dPds[mub_,Trmf_?NumericQ,para_,\[Sigma]_?NumericQ,w0s_?NumericQ,r03s_?NumericQ,\[Mu]es_?NumericQ,verb_?BooleanQ]:=Module[{reslist,Eq1,Eq2,Eq3,Eq4,Eq5,s,ss,w,rr,w0,r03,\[Mu]e},
set$couplings[para];
If[verb==True,If[Trmf<= .1,Print["Temperature is either 0 or T< 1MeV, T too small for finite T numerical integration, T=0 RMF solver is used"]]];

Eq1[s_,w0_,r03_,\[Mu]b_,\[Mu]e_]:=-(gs[[1]]*ns[Trmf,\[Mu]b,1,s,w0,r03]+gs[[2]]*ns[Trmf,\[Mu]b-\[Mu]e,2,s,w0,r03])-D[Lmes[ss,w0,r03],ss]/.{ss->s};
Eq2[w0_,r03_,\[Mu]b_,\[Mu]e_]:=-(gw[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gw[[2]]*nB[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w,r03],w]/.{w->w0};
Eq3[w0_,r03_,\[Mu]b_,\[Mu]e_]:=-(gr[[1]]*I3[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gr[[2]]*I3[[2]]*nB[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w0,rr],rr]/.{rr->r03};
Eq5[w0_,r03_,\[Mu]b_,\[Mu]e_]:=n$electron[Trmf,\[Mu]e]-nB[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03];

reslist=FindRoot[{Eq2[w0,r03,mub,\[Mu]e]==0,Eq3[w0,r03,mub,\[Mu]e]==0,Eq5[w0,r03,mub,\[Mu]e]==0},{{w0,Re[w0s]},{r03,Re[r03s]},{\[Mu]e,Re[\[Mu]es]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];

Return[Eq1[\[Sigma],w0,r03,mub,\[Mu]e]/.reslist]
]
(*RMFsolver at given density and for difference between dmu=mun-mup-mue neutron, electron and proton chemical potential (how far out of cold beta equil), automatically enforces charge neutrality*)
RMFsolve$nonequil[nbext_,dmu_,Trmf_?NumericQ,para_,\[Sigma]s_?NumericQ,w0s_?NumericQ,r03s_?NumericQ,\[Mu]es_?NumericQ,mubs_?NumericQ,verb_?BooleanQ]:=Module[{reslist,Eq1,Eq2,Eq3,Eq4,Eq5,s,w,rr,\[Sigma],w0,r03,\[Mu]e,mub},
set$couplings[para];
If[verb==True,If[Trmf<= .1,Print["Temperature is either 0 or T< 1MeV, T too small for finite T numerical integration, T=0 RMF solver is used"]]];

Eq1[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=-(gs[[1]]*ns[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gs[[2]]*ns[Trmf,\[Mu]p,2,\[Sigma],w0,r03])-D[Lmes[s,w0,r03],s]/.{s->\[Sigma]};
Eq2[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=-(gw[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gw[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w,r03],w]/.{w->w0};
Eq3[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=-(gr[[1]]*I3[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gr[[2]]*I3[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w0,rr],rr]/.{rr->r03};
Eq4[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03]-nbext;
Eq5[\[Sigma]_,w0_,r03_,\[Mu]p_,\[Mu]e_]:=n$electron[Trmf,\[Mu]e]-nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03];

reslist=FindRoot[{Eq1[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]==0,Eq2[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]==0,Eq3[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]==0,Eq4[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]==0,Eq5[\[Sigma],w0,r03,mub-\[Mu]e-dmu,\[Mu]e]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{r03,Re[r03s]},{\[Mu]e,Re[\[Mu]es]},{mub,Re[mubs]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,r03/.reslist,mub/.reslist,((mub-\[Mu]e)/.reslist)-dmu,\[Mu]e/.reslist,0},{(Eq1[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]/.reslist),(Eq2[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]/.reslist),Eq3[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]/.reslist,Eq4[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]/.reslist,Eq5[\[Sigma],w0,r03,mub-\[Mu]e-dmu,\[Mu]e]/.reslist},Trmf,nbext,para};
chk=out[[2]];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]
]
(*this is the only solver that doesn't follow the standard "out" structure for the chemical potentials! *)
RMFsolve$nonequil$xpconst[nbext_?NumericQ,dmu_?NumericQ,Trmf_?NumericQ,para_,\[Sigma]s_?NumericQ,w0s_?NumericQ,r03s_?NumericQ,\[Mu]es_?NumericQ,mubs_?NumericQ,verb_?BooleanQ]:=Module[{reslist,Eq1,Eq2,Eq3,Eq4,Eq5,s,w,rr,\[Sigma],w0,r03,\[Mu]e,mub,xp,out,chk},
set$couplings[para];
xp=proton$fraction$RMF[RMFsolve[nbext,Trmf,para,\[Sigma]s,w0s,r03s,mubs,\[Mu]es,False]];
If[verb==True,If[Trmf<= .1,Print["Temperature is either 0 or T< 1MeV, T too small for finite T numerical integration, T=0 RMF solver is used"]]];

Eq1[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=-(gs[[1]]*ns[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gs[[2]]*ns[Trmf,\[Mu]p,2,\[Sigma],w0,r03])-D[Lmes[s,w0,r03],s]/.{s->\[Sigma]};
Eq2[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=-(gw[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gw[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w,r03],w]/.{w->w0};
Eq3[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=-(gr[[1]]*I3[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gr[[2]]*I3[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w0,rr],rr]/.{rr->r03};
Eq4[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03]/(nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03]+nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03])-xp;
Eq5[\[Sigma]_,w0_,r03_,\[Mu]p_,\[Mu]e_]:=n$electron[Trmf,\[Mu]e]-nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03];

reslist=FindRoot[{Eq1[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]==0,Eq2[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]==0,Eq3[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]==0,Eq4[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]==0,Eq5[\[Sigma],w0,r03,mub-\[Mu]e-dmu,\[Mu]e]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{r03,Re[r03s]},{\[Mu]e,Re[\[Mu]es]},{mub,Re[mubs]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,r03/.reslist,mub/.reslist,((mub-\[Mu]e)/.reslist)-dmu,\[Mu]e/.reslist,(*here should be ,0 ,for munu but this will break some notebooks*)((nB[Trmf,((mub-\[Mu]e)/.reslist)-dmu,2,\[Sigma],w0,r03]/.reslist)+(nB[Trmf,mub,1,\[Sigma],w0,r03]/.reslist))-nbext},{(Eq1[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]/.reslist),(Eq2[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]/.reslist),Eq3[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]/.reslist,Eq4[\[Sigma],w0,r03,mub,mub-\[Mu]e-dmu]/.reslist,Eq5[\[Sigma],w0,r03,mub-\[Mu]e-dmu,\[Mu]e]/.reslist},Trmf,nbext,para};
chk=out[[2]];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]
]
(*RMFsolver out of equilibrium at given T,nb and xp, enforces charge neutrality*)
Options[RMFsolve$xp]={electrons->True}
RMFsolve$xp[nbext_?NumericQ,xp_?NumericQ,Trmf_?NumericQ,para_,\[Sigma]s_?NumericQ,w0s_?NumericQ,r03s_?NumericQ,mubs_?NumericQ,mups_?NumericQ,\[Mu]es_?NumericQ,verb_?BooleanQ,OptionsPattern[]]:=Module[{reslist,Eq1,Eq2,Eq3,Eq4,Eq5,Eq6,s,w,rr,\[Sigma],w0,r03,\[Mu]e,mub,mup,out,chk},
set$couplings[para];
If[verb==True,If[Trmf<= .1,Print["Temperature is either 0 or T< 1MeV, T too small for finite T numerical integration, T=0 RMF solver is used"]]];

Eq1[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=-(gs[[1]]*ns[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gs[[2]]*ns[Trmf,\[Mu]p,2,\[Sigma],w0,r03])-D[Lmes[s,w0,r03],s]/.{s->\[Sigma]};
Eq2[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=-(gw[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gw[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w,r03],w]/.{w->w0};
Eq3[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=-(gr[[1]]*I3[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gr[[2]]*I3[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w0,rr],rr]/.{rr->r03};
Eq4[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03]/(nbext)-xp;
Eq5[\[Sigma]_,w0_,r03_,\[Mu]p_,\[Mu]e_]:=n$electron[Trmf,\[Mu]e]-nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03];
Eq6[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_,\[Mu]e_]:=nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03]+nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]-nbext;
If[OptionValue[electrons],
reslist=FindRoot[{Eq1[\[Sigma],w0,r03,mub,mup]==0,Eq2[\[Sigma],w0,r03,mub,mup]==0,Eq3[\[Sigma],w0,r03,mub,mup]==0,Eq4[\[Sigma],w0,r03,mub,mup]==0,Eq5[\[Sigma],w0,r03,mup,\[Mu]e]==0,Eq6[\[Sigma],w0,r03,mub,mup,\[Mu]e]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{r03,Re[r03s]},{\[Mu]e,Re[\[Mu]es]},{mub,Re[mubs]},{mup,Re[mups]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,r03/.reslist,mub/.reslist,mup/.reslist,\[Mu]e/.reslist,0},{(Eq1[\[Sigma],w0,r03,mub,mup]/.reslist),(Eq2[\[Sigma],w0,r03,mub,mup]/.reslist),Eq3[\[Sigma],w0,r03,mub,mup]/.reslist,Eq4[\[Sigma],w0,r03,mub,mup]/.reslist,Eq5[\[Sigma],w0,r03,mup,\[Mu]e]/.reslist,Eq6[\[Sigma],w0,r03,mub,mup,\[Mu]e]/.reslist},Trmf,nbext,para};
chk=out[[2]]; ,
reslist=FindRoot[{Eq1[\[Sigma],w0,r03,mub,mup]==0,Eq2[\[Sigma],w0,r03,mub,mup]==0,Eq3[\[Sigma],w0,r03,mub,mup]==0,Eq4[\[Sigma],w0,r03,mub,mup]==0,Eq6[\[Sigma],w0,r03,mub,mup,\[Mu]e]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{r03,Re[r03s]},{mub,Re[mubs]},{mup,Re[mups]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,r03/.reslist,mub/.reslist,mup/.reslist,0,0},{(Eq1[\[Sigma],w0,r03,mub,mup]/.reslist),(Eq2[\[Sigma],w0,r03,mub,mup]/.reslist),Eq3[\[Sigma],w0,r03,mub,mup]/.reslist,Eq4[\[Sigma],w0,r03,mub,mup]/.reslist,0,Eq6[\[Sigma],w0,r03,mub,mup,\[Mu]e]/.reslist},Trmf,nbext,para};
chk=out[[2]];];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]
]
(*RMFsolver out of equilibrium at given T,mu_B and xp, enforces charge neutrality*)
RMFsolve$muB$muQ[mub_?NumericQ,\[Mu]e_?NumericQ,Trmf_?NumericQ,para_,\[Sigma]s_?NumericQ,w0s_?NumericQ,r03s_?NumericQ,mups_?NumericQ,verb_?BooleanQ]:=Module[{reslist,Eq1,Eq2,Eq3,Eq4,Eq5,s,w,rr,\[Sigma],w0,r03,out,chk,mup},
set$couplings[para];

If[verb==True,If[Trmf<= .1,Print["Temperature is either 0 or T< 1MeV, T too small for finite T numerical integration, T=0 RMF solver is used"]]];

Eq1[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=-(gs[[1]]*ns[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gs[[2]]*ns[Trmf,\[Mu]p,2,\[Sigma],w0,r03])-D[Lmes[s,w0,r03],s]/.{s->\[Sigma]};
Eq2[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=-(gw[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gw[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w,r03],w]/.{w->w0};
Eq3[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=-(gr[[1]]*I3[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gr[[2]]*I3[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w0,rr],rr]/.{rr->r03};
(*Eq4[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]p_]:=nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03]/(nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03]+nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03])-xp;*)
Eq5[\[Sigma]_,w0_,r03_,\[Mu]p_]:=n$electron[Trmf,\[Mu]e]-nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03];

reslist=FindRoot[{Eq1[\[Sigma],w0,r03,mub,mup]==0,Eq2[\[Sigma],w0,r03,mub,mup]==0,Eq3[\[Sigma],w0,r03,mub,mup]==0,Eq5[\[Sigma],w0,r03,mup]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{r03,Re[r03s]},{mup,Re[mups]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,r03/.reslist,mub,mup/.reslist,\[Mu]e,0},{(Eq1[\[Sigma],w0,r03,mub,mup]/.reslist),(Eq2[\[Sigma],w0,r03,mub,mup]/.reslist),Eq3[\[Sigma],w0,r03,mub,mup]/.reslist,0,Eq5[\[Sigma],w0,r03,mup]/.reslist},Trmf,(nB[Trmf,mub,1,\[Sigma],w0,r03]+nB[Trmf,mup,2,\[Sigma],w0,r03])/.reslist,para};
chk=out[[2]];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]
]



(* ::Subtitle:: *)
(*Neutrino Trapped npe-Matter (chemical equilibrium mu_n+mu_nu=mu_p+mu_e, charge neutral)*)


(*charge neutral, beta equilibrated, fixed lepton number Yl, at given nB*)
RMFsolve$nu[nbext_?NumericQ,Trmf_?NumericQ,Yl_?NumericQ,para_,\[Sigma]s_?NumericQ,w0s_?NumericQ,r03s_?NumericQ,\[Mu]bs_?NumericQ,\[Mu]es_?NumericQ,\[Mu]nus_?NumericQ,verb_?BooleanQ]:=Module[{reslist,Eq1,Eq2,Eq3,Eq4,Eq5,Eq6,s,w,rr,\[Sigma],w0,r03,\[Mu]n,\[Mu]e,\[Mu]nu},
set$couplings[para];
If[verb==True,If[Trmf<= .1,Print["Temperature is either 0 or T< .1MeV, T too small for finite T numerical integration, T=0 RMF solver is used"]]];

Eq1[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]e_,\[Mu]nu_]:=-(gs[[1]]*ns[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+gs[[2]]*ns[Trmf,\[Mu]n-\[Mu]e+\[Mu]nu,2,\[Sigma],w0,r03])-D[Lmes[s,w0,r03],s]/.{s->\[Sigma]};(*dP/d\[Sigma]=0*)
Eq2[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]e_,\[Mu]nu_]:=-(gw[[1]]*nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+gw[[2]]*nB[Trmf,\[Mu]n-\[Mu]e+\[Mu]nu,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w,r03],w]/.{w->w0};(*dP/Subscript[d\[Omega], 0]=0*)
Eq3[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]e_,\[Mu]nu_]:=-(gr[[1]]*I3[[1]]*nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+gr[[2]]*I3[[2]]*nB[Trmf,\[Mu]n-\[Mu]e+\[Mu]nu,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w0,rr],rr]/.{rr->r03};(*dP/Subscript[d\[Rho], 03]=0*)
Eq4[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]e_,\[Mu]nu_]:=nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+nB[Trmf,\[Mu]n-\[Mu]e+\[Mu]nu,2,\[Sigma],w0,r03]-nbext;(*solve at fixed nB*)
Eq5[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]e_,\[Mu]nu_]:=n$electron[Trmf,\[Mu]e]-nB[Trmf,\[Mu]n-\[Mu]e+\[Mu]nu,2,\[Sigma],w0,r03]; (*enforce charge neutrality*)
Eq6[\[Mu]e_,\[Mu]nu_]:=n$electron[Trmf,\[Mu]e]+n$neutrino[Trmf,\[Mu]nu]-Yl*nbext;(*solve at given lepton number*)
reslist=FindRoot[{Eq1[\[Sigma],w0,r03,\[Mu]n,\[Mu]e,\[Mu]nu]==0,Eq2[\[Sigma],w0,r03,\[Mu]n,\[Mu]e,\[Mu]nu]==0,Eq3[\[Sigma],w0,r03,\[Mu]n,\[Mu]e,\[Mu]nu]==0,Eq4[\[Sigma],w0,r03,\[Mu]n,\[Mu]e,\[Mu]nu]==0,Eq5[\[Sigma],w0,r03,\[Mu]n,\[Mu]e,\[Mu]nu]==0,Eq6[\[Mu]e,\[Mu]nu]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{r03,Re[r03s]},{\[Mu]e,Re[\[Mu]es]},{\[Mu]n,Re[\[Mu]bs]},{\[Mu]nu,Re[\[Mu]nus]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,r03/.reslist,\[Mu]n/.reslist,(\[Mu]n-\[Mu]e+\[Mu]nu)/.reslist,\[Mu]e/.reslist,\[Mu]nu/.reslist},{(Eq1[\[Sigma],w0,r03,\[Mu]n,\[Mu]e,\[Mu]nu]/.reslist),(Eq2[\[Sigma],w0,r03,\[Mu]n,\[Mu]e,\[Mu]nu]/.reslist),Eq3[\[Sigma],w0,r03,\[Mu]n,\[Mu]e,\[Mu]nu]/.reslist,Eq4[\[Sigma],w0,r03,\[Mu]n,\[Mu]e,\[Mu]nu]/.reslist,Eq5[\[Sigma],w0,r03,\[Mu]n,\[Mu]e,\[Mu]nu]/.reslist,Eq6[\[Mu]e,\[Mu]nu]/.reslist},Trmf,nbext,para};
chk=out[[2]];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]
]

(*charge neutral, beta equilibrated, fixed lepton number Yl, at given range of nB*)
RMFsolve$nu$nb[nbstart_?NumericQ,nbeend_?NumericQ,nbpoints_?NumericQ,T_?NumericQ,Yl_?NumericQ,para_,\[Sigma]s_?NumericQ,w0s_?NumericQ,r03s_?NumericQ,\[Mu]bs_?NumericQ,\[Mu]es_?NumericQ,\[Mu]nus_?NumericQ,verb_?BooleanQ]:=Module[{nbrestab,ss,ws,rs,mus,mues,nbs,nbe,nbp,dnb,nsat,munus},
ss=\[Sigma]s;ws=w0s;rs=r03s;mus=\[Mu]bs;mues=\[Mu]es;munus=\[Mu]nus;nbrestab={};nbs=nbstart;nbe=nbeend;nbp=nbpoints;dnb=(nbe-nbs)/nbp;
Do[((*Print[nbs+(i-1)dnb];*)
nbrestab=Append[nbrestab,{nbs+(i-1)dnb,Quiet[RMFsolve$nu[(nbs+(i-1)dnb),T,Yl,para,ss,ws,rs,mus,mues,munus,verb]]}];
ss=Re[nbrestab[[i,2,1,1]]];
ws=Re[nbrestab[[i,2,1,2]]];
rs=Re[nbrestab[[i,2,1,3]]];
mus=Re[nbrestab[[i,2,1,4]]];
mues=Re[nbrestab[[i,2,1,6]]];
munus=Re[nbrestab[[i,2,1,7]]];
)
,{i,1,nbp+1,1}];
Return[nbrestab]
]



(* ::Subtitle::Initialization:: *)
(*Beta Equilibrated npe matter , charge neutrality imposed*)


(* ::Input::Initialization:: *)
(*RMF solving module for beta equilibrated nuclear matter + e;ectrons (mun=mup+mue), inputs are baryon density, temperature, coupling constants for the RMF model in para and initial guesses. For T<1MeV a T=0 solver is used, for higher T the full T dependence is taken into account. Output is in form of replacement rules + checks + T+ RMFmodel couplings *)

RMFsolve[nbext_?NumericQ,Trmf_?NumericQ,para_,\[Sigma]s_?NumericQ,w0s_?NumericQ,r03s_?NumericQ,\[Mu]bs_?NumericQ,\[Mu]es_?NumericQ,verb_?BooleanQ]:=Module[{reslist,Eq1,Eq2,Eq3,Eq4,Eq5,s,w,rr,\[Sigma],w0,r03,\[Mu]b,\[Mu]e},
set$couplings[para];
If[verb==True,If[Trmf<= .1,Print["Temperature is either 0 or T< .1MeV, T too small for finite T numerical integration, T=0 RMF solver is used"]]];

Eq1[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]e_]:=-(gs[[1]]*ns[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gs[[2]]*ns[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03])-D[Lmes[s,w0,r03],s]/.{s->\[Sigma]};
Eq2[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]e_]:=-(gw[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gw[[2]]*nB[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w,r03],w]/.{w->w0};
Eq3[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]e_]:=-(gr[[1]]*I3[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gr[[2]]*I3[[2]]*nB[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w0,rr],rr]/.{rr->r03};
Eq4[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]e_]:=nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+nB[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03]-nbext;
Eq5[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]e_]:=n$electron[Trmf,\[Mu]e]-nB[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03];
reslist=FindRoot[{Eq1[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]==0,Eq2[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]==0,Eq3[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]==0,Eq4[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]==0,Eq5[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{r03,Re[r03s]},{\[Mu]e,Re[\[Mu]es]},{\[Mu]b,Re[\[Mu]bs]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,r03/.reslist,\[Mu]b/.reslist,(\[Mu]b-\[Mu]e)/.reslist,(\[Mu]e)/.reslist,0},{(Eq1[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]/.reslist),(Eq2[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]/.reslist),Eq3[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]/.reslist,Eq4[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]/.reslist,Eq5[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]/.reslist},Trmf,nbext,para};
chk=out[[2]];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]
]

(*module to solve RMF for various densities and automatically updating the initial guesses*)
RMFsolve$nb[nbstart_,nbeend_,nbpoints_,T_,para_,\[Sigma]s_,w0s_,r03s_,\[Mu]bs_,\[Mu]es_,verb_?BooleanQ]:=Module[{nbrestab,ss,ws,rs,mus,mues,nbs,nbe,nbp,dnb,nsat},
ss=\[Sigma]s;ws=w0s;rs=r03s;mus=\[Mu]bs;mues=\[Mu]es;nbrestab={};nbs=nbstart;nbe=nbeend;nbp=nbpoints;dnb=(nbe-nbs)/nbp;
Do[((*Print[nbs+(i-1)dnb];*)
nbrestab=Append[nbrestab,{nbs+(i-1)dnb,Quiet[RMFsolve[(nbs+(i-1)dnb),T,para,ss,ws,rs,mus,mues,verb]]}];
ss=Re[nbrestab[[i,2,1,1]]];
ws=Re[nbrestab[[i,2,1,2]]];
rs=Re[nbrestab[[i,2,1,3]]];
mus=Re[nbrestab[[i,2,1,4]]];
mues=Abs[Re[nbrestab[[i,2,1,4]]]-Re[nbrestab[[i,2,1,5]]]];

)
,{i,1,nbp+1,1}];
Return[nbrestab]
]

(*module to solve RMF AND compute pressure for beta-equilibrated charge neutral npe matter*)
Options[RMFpressure]={add$photons->False}(*photon contributions at finite T are set to False by default and can be added by calling it with "add$photons->True"*)
RMFpressure[nbext_,Trmf_,para_,\[Sigma]s_,w0s_,r03s_,\[Mu]bs_,\[Mu]es_,verb_?BooleanQ,OptionsPattern[]]:=Module[{meson$pressure,nucpress,vevs,sig,w0,r03,mub,mue,k,BI,muv,chk,P$photon},
vevs=RMFsolve[nbext,Trmf,para,\[Sigma]s,w0s,r03s,\[Mu]bs,\[Mu]es,verb];
chk=vevs[[2]];
sig=vevs[[1,1]];
w0=vevs[[1,2]];
r03=vevs[[1,3]];
mub=vevs[[1,4]];
mue=vevs[[1,4]]-vevs[[1,5]];
muv={mub,mub-mue};
meson$pressure=Lmes[sig,w0,r03];
nucpress=Sum[nucleon$pressure[Trmf,muv[[BI]],sig,w0,r03,BI],{BI,1,2,1}];
P$photon=If[OptionValue[add$photons],photon$pressure[Trmf],0];
If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[meson$pressure+nucpress+electron$pressure[Trmf,mue]+P$photon+P$offset]]

]



(* ::Subtitle:: *)
(*Thermodynamic Functions*)


(* ::Input::Initialization:: *)
(*module to compute pressure for given solution of RMF obtained by RMFsolver*)
(*soltab is input created directly from RMVsolve,RMVsolvePNM or RMVsolveSYM*)
(*photon contributions at finite T are set to False by default and can be added by calling it with "add$photons->True"*)
Options[pressure$RMF]={add$photons->False,renorm->False,electrons->True, neutrinos->False}
pressure$RMF[soltab_,OptionsPattern[]]:=Module[{T,para,sig,w0,r03,mun,mup,muv,mue,meson$pressure,nucpress,munu,P$nu,P$elec,P$photon},
para=soltab[[5]];
set$couplings[para];
T=soltab[[3]];
sig=soltab[[1,1]];
w0=soltab[[1,2]];
r03=soltab[[1,3]];
mun=soltab[[1,4]];
mup=soltab[[1,5]];
mue=If[NumericQ[soltab[[1,6]]],mue=soltab[[1,6]],If[mup>0,mun-mup,0]]; (*this is sketchy, it only works for PNM,SYM, and charge neutral beta equilibrated matter, but might fail in more general settings!*)
munu=If[NumericQ[soltab[[1,7]]],munu=soltab[[1,7]],0];
muv={mun,mup};
meson$pressure=If[OptionValue[renorm],Lmes$renorm[sig,w0,r03],Lmes[sig,w0,r03]];
(*this allows for the contribution of thermal electrons and neutrinos, even when their respective chemical potentials are zero*)
If[OptionValue[electrons],P$elec=electron$pressure[T,mue],P$elec=0];

If[OptionValue[neutrinos],P$nu=neutrino$pressure[T,munu],P$nu=0];
If[OptionValue[electrons]==False&&mue!=0,Print["WARNING: electrons have finite chemical potential but electron flag = False"]];
If[OptionValue[neutrinos]==False&&munu!=0,Print["WARNING: neutrinos have finite chemical potential but neutrino flag = False"]];
P$photon=If[OptionValue[add$photons],photon$pressure[T],0];
nucpress=If[mup>0,Sum[nucleon$pressure[T,muv[[BI]],sig,w0,r03,BI],{BI,1,2,1}],nucleon$pressure[T,mun,sig,w0,r03,1]];
Return[meson$pressure+nucpress+P$elec+P$photon+P$nu+P$offset];
]
(*module to compute energy density for given solution of RMF obtained by RMFsolver*)
(*soltab is input created directly from RMVsolve,RMVsolvePNM or RMVsolveSYM*)
Options[edens$RMF]={add$photons->False,renorm->False,electrons->True, neutrinos->False}; (*the renorm option only changes the thermodynamic edens, so the function will indicate a discrepancy*)(*photon contributions at finite T are set to False by default and can be added by calling it with "add$photons->True"*)
edens$RMF[soltab_,up_Integer:4000,boundmult_Integer:20,OptionsPattern[]]:=Module[{T,para,sig,w0,r03,mun,mup,totpress,edens,mue,edens$nucleons,edens$electron,edens$neutrino,muv,entr,mespart,munu,edens$total,edens$thermo,E$photon,ov},
para=soltab[[5]];
set$couplings[para];
T=soltab[[3]];
sig=soltab[[1,1]];
w0=soltab[[1,2]];
r03=soltab[[1,3]];
mun=soltab[[1,4]];
mup=soltab[[1,5]];
mue=If[NumericQ[soltab[[1,6]]],mue=soltab[[1,6]],If[mup>0,mun-mup,0]]; (*UPDATE: this should now work for all solvers since soltab[[1,6]] will be mu_e in all "out" tables. OLD:this is sketchy, it only works for PNM,SYM, and charge neutral beta equilibrated matter*)
munu=If[NumericQ[soltab[[1,7]]],munu=soltab[[1,7]],0];
muv={mun,mup};
(*compute energy density from thermodynamic relation e=-P+Ts+mu_i*n_i*)
totpress=pressure$RMF[soltab,add$photons->OptionValue[add$photons],renorm->OptionValue[renorm],electrons->OptionValue[electrons],neutrinos->OptionValue[neutrinos]];
entr=entropy$RMF[soltab,boundmult,add$photons->OptionValue[add$photons],electrons->OptionValue[electrons],neutrinos->OptionValue[neutrinos]];
If[OptionValue[electrons]==False&&mue!=0,Print["WARNING: electrons have finite chemical potential but electron flag = False"]];
If[OptionValue[neutrinos]==False&&munu!=0,Print["WARNING: neutrinos have finite chemical potential but neutrino flag = False"]];
edens$thermo=If[T<=0.1,-(totpress-mun*nB[T,mun,1,sig,w0,r03]-mup*nB[T,mup,2,sig,w0,r03]-If[OptionValue[electrons],(mue)*n$electron[T,mue],0]-munu*n$neutrino[T,munu]),-(totpress-mun*nB[T,mun,1,sig,w0,r03]-mup*nB[T,mup,2,sig,w0,r03]-If[OptionValue[electrons],(mue)*n$electron[T,mue],0]-munu*n$neutrino[T,munu])+T*entr];
(*compute edens for all particle species directly and check thermodynamic consistency*)
mespart=Lmes[sig,w0,r03]+2U$sigma[sig]+gr[[1]]^2*r03^2(2b1*w0^2+4b2*w0^4+6b3*w0^6)+2zet/24*gw[[1]]^4*w0^4+2xi/24*gr[[1]]^4*r03^4; (*compute meson contributions from energy momentum tensor T^00 to obtain correct signs!*)
edens$nucleons=If[T<0.10,Sum[1/(8\[Pi]^2)*((2kf$bar[sig,w0,r03,muv[[BI]],BI]^3+(mB[[BI]]-gs[[BI]]*sig)^2*kf$bar[sig,w0,r03,muv[[BI]],BI])*Ef$bar[sig,w0,r03,muv[[BI]],BI]-(mB[[BI]]-gs[[BI]]*sig)^4*Log[(Ef$bar[sig,w0,r03,muv[[BI]],BI]+kf$bar[sig,w0,r03,muv[[BI]],BI])/(mB[[BI]]-gs[[BI]]*sig)]),{BI,1,2}],Sum[8\[Pi]/(2\[Pi])^3NIntegrate[Sum[k^2*(Sqrt[k^2+(mB[[BI]]-gs[[BI]]*sig)^2](*-sgn(-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03)*))/(1+Exp[(Sqrt[k^2+(mB[[BI]]-gs[[BI]]*sig)^2]-sgn(muv[[BI]]-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03))/T]),{BI,1,2}],{k,0,up}],{sgn,-1,1,2}]];
edens$electron=If[OptionValue[electrons],If[T<=0.1,+mue^4/(4\[Pi]^2),Sum[+8\[Pi]/(2\[Pi])^3*NIntegrate[k^2*Sqrt[k^2+me^2]/(1+Exp[(Sqrt[k^2+me^2]-sgn*mue)/T]),{k,0,up}],{sgn,-1,1,2}]],0];
ov=OptionValue[add$photons];
E$photon=If[ov,photon$energy[T],0];
edens$neutrino=If[OptionValue[neutrinos],neutrino$edens[T,munu,up],0];
edens$total=edens$nucleons+edens$electron+mespart+E$photon+edens$neutrino;
Return[{edens$thermo,edens$total-P$offset,(edens$thermo-edens$total+P$offset)/(edens$total-P$offset)}]
];
(*module to compute entropy density at finite T*)
Options[entropy$RMF]={add$photons->False,integration$method->"LocalAdaptive",electrons->True,neutrinos->False};
entropy$RMF[soltab_,boundmult_Integer:40,OptionsPattern[]]:=Module[{T,para,sig,w0,r03,mun,mup,muv,mue,meson$pressure,nucpress,me,nb,kfp,kfn,xp,kfe,kfv,kfnu,entr$nucl,entr$neutrino,entr$electron,munu,entropy$total,entr$photon,ov,meth},
para=soltab[[5]];
set$couplings[para];
ov=OptionValue[add$photons];
meth=OptionValue[integration$method];
T=soltab[[3]];
nb=soltab[[4]];
sig=soltab[[1,1]];
w0=soltab[[1,2]];
r03=soltab[[1,3]];
mun=soltab[[1,4]];
mup=soltab[[1,5]];
me=0.511; (*electron mass in MeV, this should be added to para, not hardcoded*)
mue=If[NumericQ[soltab[[1,6]]],mue=soltab[[1,6]],If[mup>0,mun-mup,0]]; 
munu=If[NumericQ[soltab[[1,7]]],munu=soltab[[1,7]],0]; (*all solvers should return mu_e and mu_nu now so this should work*)
muv={mun,mup};
xp=proton$fraction$RMF[soltab];
kfp=(3*\[Pi]^2*xp*nb)^(1/3);
kfn=(3*\[Pi]^2*(1-xp)*nb)^(1/3);kfv={kfn,kfp};
kfe=(3*\[Pi]^2*n$electron[T,mue])^(1/3);
kfnu=(3*\[Pi]^2*n$neutrino[T,munu])^(1/3);
entr$nucl=If[T>0,Sum[-2/(2\[Pi])^3*4\[Pi]*Sum[NIntegrate[k^2((1-fd$bar[k,sig,sgn*w0,sgn*r03,sgn*muv[[BI]],BI,T])Log[1-fd$bar[k,sig,sgn*w0,sgn*r03,sgn*muv[[BI]],BI,T]]+fd$bar[k,sig,sgn*w0,sgn*r03,sgn*muv[[BI]],BI,T]*Log[fd$bar[k,sig,sgn*w0,sgn*r03,sgn*muv[[BI]],BI,T]]),{k,Max[0,kfv[[BI]]-boundmult*T],kfv[[BI]]+boundmult*T},Method->{meth, MaxPoints->10^7},AccuracyGoal->10],{BI,1,2}],{sgn,-1,+1,2}],0];

entr$electron=If[OptionValue[electrons],If[T>0,Sum[-2/(2\[Pi])^3*4\[Pi]*NIntegrate[k^2*(fd[Sqrt[k^2+me^2],sgn*mue,-T]*Log[fd[Sqrt[k^2+me^2],sgn*mue,-T]]+fd[Sqrt[k^2+me^2],sgn*mue,T]*Log[fd[Sqrt[k^2+me^2],sgn*mue,T]]),{k,Max[0,kfe-boundmult*T],kfe+boundmult*T}],{sgn,-1,+1,2}],0],0];
entr$neutrino=If[OptionValue[neutrinos],neutrino$entropy[T,munu,4000],0];
entr$photon=If[ov,photon$entropy[T],0];
If[OptionValue[electrons]==False&&mue!=0,Print["WARNING: electrons have finite chemical potential but electron flag = False"]];
If[OptionValue[neutrinos]==False&&munu!=0,Print["WARNING: neutrinos have finite chemical potential but neutrino flag = False"]];
entropy$total=entr$photon+entr$nucl+entr$electron+entr$neutrino;
Return[entropy$total];
];

Options[freeener$RMF]={add$photons->False,electrons->True,neutrinos->False}
freeener$RMF[soltab_,OptionsPattern[]]:=Module[{freeener,totpress,BI,T,sig,w0,r03,mun,mup,mue,muv,munu},
totpress=pressure$RMF[soltab,add$photons->OptionValue[add$photons],electrons->OptionValue[electrons],neutrinos->OptionValue[neutrinos]];
para=soltab[[5]];
set$couplings[para];
T=soltab[[3]];
sig=soltab[[1,1]];
w0=soltab[[1,2]];
r03=soltab[[1,3]];
mun=soltab[[1,4]];
mup=soltab[[1,5]];
mue=If[NumericQ[soltab[[1,6]]],mue=soltab[[1,6]],If[mup>0,mun-mup,0]]; 
munu=If[NumericQ[soltab[[1,7]]],munu=soltab[[1,7]],0]; (*all solvers should return mu_e and mu_nu now so this should work*)
muv={mun,mup};
freeener=-totpress+Sum[muv[[BI]]*nB[T,muv[[BI]],BI,sig,w0,r03],{BI,1,2}]+If[OptionValue[electrons],(mue)*n$electron[T,mue],0]+If[OptionValue[add$photons],photon$freeener[T],0]+If[OptionValue[neutrinos],munu*n$neutrino[T,munu],0];
If[OptionValue[electrons]==False&&mue!=0,Print["WARNING: electrons have finite chemical potential but electron flag = False"]];
If[OptionValue[neutrinos]==False&&munu!=0,Print["WARNING: neutrinos have finite chemical potential but neutrino flag = False"]];
Return[freeener]]

(*module to compute EOS P(eps) for given solution of RMF obtained by RMFsolver*)
(*soltab is input created directly from RMFsolve,RMFsolvePNM or RMFsolveSYM*)
Options[create$EOS]={add$photons->False,electrons->True,neutrinos->False}
create$EOS[soltab_,OptionsPattern[]]:=Module[{T,para,sig,w0,r03,mun,mup,totpress,edens,mue,mub,munu,muv},
para=soltab[[5]];
set$couplings[para];
T=soltab[[3]];
sig=soltab[[1,1]];
w0=soltab[[1,2]];
r03=soltab[[1,3]];
mun=soltab[[1,4]];
mup=soltab[[1,5]];
mue=If[NumericQ[soltab[[1,6]]],mue=soltab[[1,6]],If[mup>0,mun-mup,0]]; (*this is sketchy, it only works for PNM,SYM, and charge neutral beta equilibrated matter, but might fail in more general settings!*)
munu=If[NumericQ[soltab[[1,7]]],munu=soltab[[1,7]],0]; (*all solvers should return mu_e and mu_nu now so this should work*)
muv={mun,mup};
totpress=pressure$RMF[soltab,add$photons->OptionValue[add$photons],electrons->OptionValue[electrons],neutrinos->OptionValue[neutrinos]];
edens=edens$RMF[soltab,add$photons->OptionValue[add$photons],electrons->OptionValue[electrons],neutrinos->OptionValue[neutrinos]][[1]];
If[OptionValue[electrons]==False&&mue!=0,Print["WARNING: electrons have finite chemical potential but electron flag = False"]];
If[OptionValue[neutrinos]==False&&munu!=0,Print["WARNING: neutrinos have finite chemical potential but neutrino flag = False"]];
Return[{totpress,edens,mun}]

]
(*module to compute binding energy for given solution of RMF obtained by RMFsolver,RMVsolvePNM or RMVsolveSYM*)
(*soltab is input created directly from RMVsolveSYM*)
Options[binding$energy$RMF]={add$photons->False,renorm->False,electrons->True,neutrinos->False}
binding$energy$RMF[soltab_,OptionsPattern[]]:=Module[{T,para,sig,w0,r03,mun,mup,bind,dens,mue,munu},
para=soltab[[5]];
set$couplings[para];
mue=If[NumericQ[soltab[[1,6]]],mue=soltab[[1,6]],If[mup>0,mun-mup,0]]; (*this is sketchy, it only works for PNM,SYM, and charge neutral beta equilibrated matter, but might fail in more general settings!*)
munu=If[NumericQ[soltab[[1,7]]],munu=soltab[[1,7]],0]; (*all solvers should return mu_e and mu_nu now so this should work*)
dens=soltab[[4]];
If[OptionValue[electrons]==False&&mue!=0,Print["WARNING: electrons have finite chemical potential but electron flag = False"]];
If[OptionValue[neutrinos]==False&&munu!=0,Print["WARNING: neutrinos have finite chemical potential but neutrino flag = False"]];
bind=((edens$RMF[soltab,add$photons->OptionValue[add$photons],electrons->OptionValue[electrons],neutrinos->OptionValue[neutrinos],renorm->OptionValue[renorm]][[1]]+P$offset)/dens-mB[[1]]);
Return[bind]

]
(*module to compute proton fraction for given solution of RMF obtained by RMFsolver*)
(*soltab is input created directly from RMVsolve*)
proton$fraction$RMF[soltab_]:=Module[{T,para,sig,w0,r03,mun,mup,xp,dens},
para=soltab[[5]];
set$couplings[para];
T=soltab[[3]];
dens=soltab[[4]];
sig=soltab[[1,1]];
w0=soltab[[1,2]];
r03=soltab[[1,3]];
mun=soltab[[1,4]];
mup=soltab[[1,5]];
xp=nB[T,mup,2,sig,w0,r03]/dens;
Return[xp]

]



(* ::Input::Initialization:: *)
(*compute dU threshold in beta equilibrated npe matter, input is para list*)
dU$threshold[para_]:=Module[{nucdens,soltab$tab,kfntab,kfptab,kfetab,momsurplus,pl,dUthr,x,dUthrout,ss},
nucdens=1.2293605098323745`*^6; (*corresponds to 0.16 fm^-3*)
ss=If[para[[4,1]]<0,ss=-30,ss=30];

soltab$tab=RMFsolve$nb[0.5nucdens,10*nucdens,100,0,para,ss,20,-3,990,100,False];
kfntab=Table[{soltab$tab[[i,1]],kf$bar[soltab$tab[[i,2,1,1]],soltab$tab[[i,2,1,2]],soltab$tab[[i,2,1,3]],soltab$tab[[i,2,1,4]],1]},{i,1,Length[soltab$tab]}];
kfptab=Table[{soltab$tab[[i,1]],kf$bar[soltab$tab[[i,2,1,1]],soltab$tab[[i,2,1,2]],soltab$tab[[i,2,1,3]],soltab$tab[[i,2,1,5]],2]},{i,1,Length[soltab$tab]}];
kfetab=Table[{soltab$tab[[i,1]],kF$electron[soltab$tab[[i,2,1,4]]-soltab$tab[[i,2,1,5]]]},{i,1,Length[soltab$tab]}];
momsurplus=Table[{kfntab[[i,1]]/nucdens,-kfntab[[i,2]]+kfptab[[i,2]]+kfetab[[i,2]]},{i,1,Length[soltab$tab]}];
pl=ListLinePlot[momsurplus,Axes->False,Frame->True,FrameLabel->{"nB/\!\(\*SubscriptBox[\(n\), \(0\)]\)","\!\(\*SubscriptBox[\(k\), \(FN\)]\)-\!\(\*SubscriptBox[\(k\), \(FP\)]\)-\!\(\*SubscriptBox[\(k\), \(FE\)]\) [MeV]"}];

dUthr=x/.FindRoot[Interpolation[momsurplus,InterpolationOrder->1][x]==0,{x,3}];
If[dUthr>10 || dUthr<0.5,dUthrout="No dU threshold found between 0.5 and 10 n0",dUthrout=dUthr];
Return[{momsurplus,pl}]]

(*SYMMETRIC NUCLEAR MATTER, NO ELECTRONS *)

(*RMF solver for symmetric nuclear matter, no electrons*)(*THIS ONLY IS CORRECT IF MN=MP!!!! FIX THAT*)
RMFsolveSYM[nbext_,Trmf_,para_,\[Sigma]s_,w0s_,\[Mu]bs_,verb_?BooleanQ]:=Module[{reslist,Eq1,Eq2,Eq3,Eq4,Eq5,s,w,rr,\[Sigma],w0,r03,\[Mu]b,EqSYM,\[Mu]n,\[Mu]p},
set$couplings[para];
Eq1[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=-(gs[[1]]*ns[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+gs[[2]]*ns[Trmf,\[Mu]p,2,\[Sigma],w0,r03])-D[Lmes[s,w0,r03],s]/.{s->\[Sigma]};
Eq2[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=-(gw[[1]]*nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+gw[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w,r03],w]/.{w->w0};
Eq3[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=-(gr[[1]]*I3[[1]]*nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+gr[[2]]*I3[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w0,rr],rr]/.{rr->r03};
Eq4[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03]-nbext;
EqSYM[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]-nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03];
If[verb==True,If[Trmf<=0.1,Print["Temperature is either 0 or T< 1MeV, T too small for finite T numerical integration, T=0 RMF solver is used"]]];
If[mB[[1]]==mB[[2]],



reslist=FindRoot[{Eq1[\[Sigma],w0,0,\[Mu]b,\[Mu]b]==0,Eq2[\[Sigma],w0,0,\[Mu]b,\[Mu]b]==0,Eq4[\[Sigma],w0,0,\[Mu]b,\[Mu]b]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{\[Mu]b,Re[\[Mu]bs]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,0,\[Mu]b/.reslist,\[Mu]b/.reslist,0,0},{(Eq1[\[Sigma],w0,0,\[Mu]b,\[Mu]b]/.reslist),(Eq2[\[Sigma],w0,0,\[Mu]b,\[Mu]b]/.reslist),Eq4[\[Sigma],w0,0,\[Mu]b,\[Mu]b]/.reslist},Trmf,nbext,para};
chk=out[[2]];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]
,
reslist=FindRoot[{Eq1[\[Sigma],w0,0,\[Mu]n,\[Mu]p]==0,Eq2[\[Sigma],w0,0,\[Mu]n,\[Mu]p]==0,Eq4[\[Sigma],w0,0,\[Mu]n,\[Mu]p]==0,EqSYM[\[Sigma],w0,0,\[Mu]n,\[Mu]p]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{\[Mu]n,Re[\[Mu]bs]},{\[Mu]p,Re[\[Mu]bs+1]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,0,\[Mu]n/.reslist,\[Mu]p/.reslist,0,0},{(Eq1[\[Sigma],w0,0,\[Mu]n,\[Mu]p]/.reslist),(Eq2[\[Sigma],w0,0,\[Mu]n,\[Mu]p]/.reslist),Eq4[\[Sigma],w0,0,\[Mu]n,\[Mu]p]/.reslist,EqSYM[\[Sigma],w0,0,\[Mu]n,\[Mu]p]/.reslist},Trmf,nbext,para};
chk=out[[2]];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]
]
]
(*module to solve isospin symmetric nuclear matter for a renormalized model of the bogota bodmer type*)
RMFsolveSYM$renorm[nbext_,Trmf_,para_,\[Sigma]s_,w0s_,\[Mu]bs_,verb_?BooleanQ]:=Module[{reslist,Eq1,Eq2,Eq3,Eq4,Eq5,s,w,rr,\[Sigma],w0,r03,\[Mu]b,EqSYM,\[Mu]n,\[Mu]p},
set$couplings[para];
Eq1[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=-(gs[[1]]*ns[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+gs[[2]]*ns[Trmf,\[Mu]p,2,\[Sigma],w0,r03])-D[Lmes$renorm[s,w0,r03],s]/.{s->\[Sigma]};
Eq2[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=-(gw[[1]]*nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+gw[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes$renorm[\[Sigma],w,r03],w]/.{w->w0};
Eq3[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=-(gr[[1]]*I3[[1]]*nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+gr[[2]]*I3[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes$renorm[\[Sigma],w0,rr],rr]/.{rr->r03};
Eq4[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03]-nbext;
EqSYM[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]-nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03];
If[verb==True,If[Trmf<=0.5,Print["Temperature is either 0 or T< 0.5MeV, T too small for finite T numerical integration, T=0 RMF solver is used"]]];
If[mB[[1]]==mB[[2]],



reslist=FindRoot[{Eq1[\[Sigma],w0,0,\[Mu]b,\[Mu]b]==0,Eq2[\[Sigma],w0,0,\[Mu]b,\[Mu]b]==0,Eq4[\[Sigma],w0,0,\[Mu]b,\[Mu]b]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{\[Mu]b,Re[\[Mu]bs]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,0,\[Mu]b/.reslist,\[Mu]b/.reslist,0,0},{(Eq1[\[Sigma],w0,0,\[Mu]b,\[Mu]b]/.reslist),(Eq2[\[Sigma],w0,0,\[Mu]b,\[Mu]b]/.reslist),Eq4[\[Sigma],w0,0,\[Mu]b,\[Mu]b]/.reslist},Trmf,nbext,para};
chk=out[[2]];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]
,
reslist=FindRoot[{Eq1[\[Sigma],w0,0,\[Mu]n,\[Mu]p]==0,Eq2[\[Sigma],w0,0,\[Mu]n,\[Mu]p]==0,Eq4[\[Sigma],w0,0,\[Mu]n,\[Mu]p]==0,EqSYM[\[Sigma],w0,0,\[Mu]n,\[Mu]p]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{\[Mu]n,Re[\[Mu]bs]},{\[Mu]p,Re[\[Mu]bs+1]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,0,\[Mu]n/.reslist,\[Mu]p/.reslist,0,0},{(Eq1[\[Sigma],w0,0,\[Mu]n,\[Mu]p]/.reslist),(Eq2[\[Sigma],w0,0,\[Mu]n,\[Mu]p]/.reslist),Eq4[\[Sigma],w0,0,\[Mu]n,\[Mu]p]/.reslist,EqSYM[\[Sigma],w0,0,\[Mu]n,\[Mu]p]/.reslist},Trmf,nbext,para};
chk=out[[2]];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]
]
]


(* module to solve SNM RMFs as density table*)
RMFsolve$nb$SYM[nbstart_,nbeend_,nbpoints_,T_,para_,\[Sigma]s_,w0s_,\[Mu]bs_,verb_?BooleanQ]:=Module[{nbrestab,ss,ws,rs,mus,mues,nbs,nbe,nbp,dnb,nsat},
ss=\[Sigma]s;ws=w0s;mus=\[Mu]bs;nbrestab={};nbs=nbstart;nbe=nbeend;nbp=nbpoints;dnb=(nbe-nbs)/nbp;
Do[((*Print[nbs+(i-1)dnb];*)
nbrestab=Append[nbrestab,{nbs+(i-1)dnb,Quiet[RMFsolveSYM[(nbs+(i-1)dnb),T,para,ss,ws,mus,verb]]}];
ss=Re[nbrestab[[i,2,1,1]]];
ws=Re[nbrestab[[i,2,1,2]]];
mus=Re[nbrestab[[i,2,1,4]]];
)
,{i,1,nbp+1,1}];
Return[nbrestab]
]
(*module to solve RMF AND compute pressure for symmetric  np matter, no electrons no photons no bag constant?*)
RMFpressureSYM[nbext_?NumericQ,Trmf_?NumericQ,para_,\[Sigma]s_?NumericQ,w0s_?NumericQ,\[Mu]bs_?NumericQ,verb_?BooleanQ]:=Module[{meson$pressure,nucpress,vevs,sig,w0,r03,mub,mue,k,BI,muv,chk},
vevs=RMFsolveSYM[nbext,Trmf,para,\[Sigma]s,w0s,\[Mu]bs,verb];
chk=vevs[[2]];
sig=vevs[[1,1]];
w0=vevs[[1,2]];
r03=0;
mub=vevs[[1,4]];
muv={mub,mub};
meson$pressure=Lmes[sig,w0,0];
nucpress=Sum[nucleon$pressure[Trmf,muv[[BI]],sig,w0,0,BI],{BI,1,2,1}];If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[meson$pressure+nucpress]],Return[meson$pressure+nucpress]]

]
(*module to solve RMF AND compute binding energy SNM*)
RMFbindingSYM[nbext_,Trmf_,para_,\[Sigma]s_,w0s_,r03s_:0,\[Mu]bs_,verb_?BooleanQ]:=Module[{meson$pressure,nucpress,vevs,sig,w0,r03,k,muv,chk,mup,mun,mub,bind
},
set$couplings[para];
vevs=RMFsolveSYM[nbext,Trmf,para,\[Sigma]s,w0s,\[Mu]bs,verb];
chk=vevs[[2]];
bind=binding$energy$RMF[vevs];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[bind]],Return[bind]]
]


(*PURE NEUTRON MATTER *)
(*RMF solving module, inputs are baryon density, temperature, coupling constants for the RMF model in para and initial guesses. For T<1MeV a T=0 solver is used, for higher T the full T dependence is taken into account. Output is in form of replacement rules + checks + T+ RMFmodel couplings *)

RMFsolvePNM[nbext_,Trmf_,para_,\[Sigma]s_,w0s_,r03s_,\[Mu]bs_,verb_?BooleanQ]:=Module[{reslist,Eq1,Eq2,Eq3,Eq4,s,w,rr,\[Sigma],w0,r03,\[Mu]b},
set$couplings[para];
If[verb==True,If[Trmf<=0.1,Print["Temperature is either 0 or T< 1MeV, T too small for finite T numerical integration, T=0 RMF solver is used"]]];

Eq1[\[Sigma]_,w0_,r03_,\[Mu]b_]:=-(gs[[1]]*ns[Trmf,\[Mu]b,1,\[Sigma],w0,r03])-D[Lmes[s,w0,r03],s]/.{s->\[Sigma]};
Eq2[\[Sigma]_,w0_,r03_,\[Mu]b_]:=-(gw[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w,r03],w]/.{w->w0};
Eq3[\[Sigma]_,w0_,r03_,\[Mu]b_]:=-(gr[[1]]*I3[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w0,rr],rr]/.{rr->r03};
Eq4[\[Sigma]_,w0_,r03_,\[Mu]b_]:=nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]-nbext;

reslist=FindRoot[{Eq1[\[Sigma],w0,r03,\[Mu]b]==0,Eq2[\[Sigma],w0,r03,\[Mu]b]==0,Eq3[\[Sigma],w0,r03,\[Mu]b]==0,Eq4[\[Sigma],w0,r03,\[Mu]b]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{r03,Re[r03s]},{\[Mu]b,Re[\[Mu]bs]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,r03/.reslist,\[Mu]b/.reslist,0,0},{(Eq1[\[Sigma],w0,r03,\[Mu]b]/.reslist),(Eq2[\[Sigma],w0,r03,\[Mu]b]/.reslist),Eq3[\[Sigma],w0,r03,\[Mu]b]/.reslist,Eq4[\[Sigma],w0,r03,\[Mu]b]/.reslist,0},Trmf,nbext,para};
chk=out[[2]];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]


]
(*module to solve PNM RMFs for density range *)
RMFsolve$nb$PNM[nbstart_,nbeend_,nbpoints_,T_,para_,\[Sigma]s_,w0s_,r03s_,\[Mu]bs_,verb_?BooleanQ]:=Module[{nbrestab,ss,ws,rs,mus,mues,nbs,nbe,nbp,dnb,nsat},
ss=\[Sigma]s;ws=w0s;rs=r03s;mus=\[Mu]bs;nbrestab={};nbs=nbstart;nbe=nbeend;nbp=nbpoints;dnb=(nbe-nbs)/nbp;
Do[((*Print[nbs+(i-1)dnb];*)
nbrestab=Append[nbrestab,{nbs+(i-1)dnb,Quiet[RMFsolvePNM[(nbs+(i-1)dnb),T,para,ss,ws,rs,mus,verb]]}];
ss=Re[nbrestab[[i,2,1,1]]];
ws=Re[nbrestab[[i,2,1,2]]];
rs=Re[nbrestab[[i,2,1,3]]];
mus=Re[nbrestab[[i,2,1,4]]];
)
,{i,1,nbp+1,1}];
Return[nbrestab]
]


(*module to solve RMF AND compute pressure PNM*)
RMFpressurePNM[nbext_,Trmf_,para_,\[Sigma]s_,w0s_,r03s_,\[Mu]bs_,verb_?BooleanQ]:=Module[{meson$pressure,nucpress,vevs,sig,w0,r03,mub,k,muv,chk},
set$couplings[para];
vevs=RMFsolvePNM[nbext,Trmf,para,\[Sigma]s,w0s,r03s,\[Mu]bs,verb];
chk=vevs[[2]];
sig=vevs[[1,1]];
w0=vevs[[1,2]];
r03=vevs[[1,3]];
mub=vevs[[1,4]];
muv={mub,0};
meson$pressure=Lmes[sig,w0,r03];
nucpress=nucleon$pressure[Trmf,muv[[1]],sig,w0,r03,1];

If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[meson$pressure+nucpress]],Return[meson$pressure+nucpress]]

]

(*module to solve RMF AND compute binding energy PNM*)
RMFbindingPNM[nbext_,Trmf_,para_,\[Sigma]s_,w0s_,r03s_,\[Mu]bs_,verb_?BooleanQ]:=Module[{meson$pressure,nucpress,vevs,sig,w0,r03,k,muv,chk,mup,mun},
set$couplings[para];
vevs=RMFsolvePNM[nbext,Trmf,para,\[Sigma]s,w0s,r03s,\[Mu]bs,verb];
chk=vevs[[2]];
sig=vevs[[1,1]];
w0=vevs[[1,2]];
r03=vevs[[1,3]];
mun=vevs[[1,4]];
mup=0;
muv={mun,mup};
bind=binding$energy$RMF[vevs];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[bind]],Return[bind]]
];

(*module to compute pressure for given solution of RMF obtained by RMFsolver- UPDATE: this module should be obsolete and not be used in new codes*)
(*soltab is input created directly from RMFsolve*)
pressure$RMF$PNM[soltab_]:=Module[{T,para,sig,w0,r03,mun,mup,muv,meson$pressure,nucpress},
para=soltab[[5]];
set$couplings[para];
T=soltab[[3]];
sig=soltab[[1,1]];
w0=soltab[[1,2]];
r03=soltab[[1,3]];
mun=soltab[[1,4]];
mup=0;
muv={mun,mup};
meson$pressure=Lmes[sig,w0,r03];
nucpress=nucleon$pressure[T,muv[[1]],sig,w0,r03,1];
Return[meson$pressure+nucpress];
];
(*This module computes the out of equilibrium chemical potential if initially beta equilibrated matter is pushed out of equilibrium by a density oscillation of magnitude deltan*)
DeltaMU[nbext_?NumericQ,deltan_,Trmf_?NumericQ,para_,\[Sigma]s_?NumericQ,w0s_?NumericQ,r03s_?NumericQ,\[Mu]bs_?NumericQ,\[Mu]es_?NumericQ,verb_?BooleanQ]:=
Module[{reslist,s,w,rr,\[Sigma],w0,r03,\[Mu]b,\[Mu]e,\[Mu]n,\[Mu]p,equres,Eq1o,Eq2o,Eq3o,Eq4o,Eq5o,Eq6o,xp},
equres=RMFsolve[nbext,Trmf,para,\[Sigma]s,w0s,r03s,\[Mu]bs,\[Mu]es,verb];
xp=proton$fraction$RMF[equres];

Eq1o[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=-(gs[[1]]*ns[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+gs[[2]]*ns[Trmf,\[Mu]p,2,\[Sigma],w0,r03])-D[Lmes[s,w0,r03],s]/.{s->\[Sigma]};
Eq2o[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=-(gw[[1]]*nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+gw[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w,r03],w]/.{w->w0};
Eq3o[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=-(gr[[1]]*I3[[1]]*nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+gr[[2]]*I3[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w0,rr],rr]/.{rr->r03};
Eq4o[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03]-(nbext+deltan);
Eq5o[\[Sigma]_,w0_,r03_,\[Mu]p_,\[Mu]e_]:=n$electron[Trmf,\[Mu]e]-nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03];
Eq6o[\[Sigma]_,w0_,r03_,\[Mu]p_]:=nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03]-xp*(nbext+deltan);
reslist=FindRoot[{Eq1o[\[Sigma],w0,r03,\[Mu]n,\[Mu]p]==0,Eq2o[\[Sigma],w0,r03,\[Mu]n,\[Mu]p]==0,Eq3o[\[Sigma],w0,r03,\[Mu]n,\[Mu]p]==0,Eq4o[\[Sigma],w0,r03,\[Mu]n,\[Mu]p]==0,Eq5o[\[Sigma],w0,r03,\[Mu]p,\[Mu]e]==0,Eq6o[\[Sigma],w0,r03,\[Mu]p]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{r03,Re[r03s]},{\[Mu]e,Re[\[Mu]es]},{\[Mu]n,Re[\[Mu]bs]},{\[Mu]p,Re[\[Mu]bs-\[Mu]es]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,r03/.reslist,\[Mu]n/.reslist,\[Mu]e/.reslist,\[Mu]p/.reslist,(\[Mu]n/.reslist)-(\[Mu]e/.reslist)-(\[Mu]p/.reslist)},{(Eq1o[\[Sigma],w0,r03,\[Mu]n,\[Mu]p]/.reslist),(Eq2o[\[Sigma],w0,r03,\[Mu]n,\[Mu]p]/.reslist),Eq3o[\[Sigma],w0,r03,\[Mu]n,\[Mu]p]/.reslist,Eq4o[\[Sigma],w0,r03,\[Mu]n,\[Mu]p]/.reslist,Eq5o[\[Sigma],w0,r03,\[Mu]p,\[Mu]e]/.reslist,Eq6o[\[Sigma],w0,r03,\[Mu]p]/.reslist},Trmf,nbext,para};
chk=out[[2]];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]
];



(* ::Subtitle:: *)
(*Thermodynamic Checks and derivatives*)


Options[ds$dT]={add$photons->False,integration$method->"LocalAdaptive",electrons->True,neutrinos->False};
ds$dT[soltab_,boundmult_Integer:40,OptionsPattern[]]:=Module[{T,dsdT,para,sig,w0,r03,mun,mup,muv,mue,meson$pressure,nucpress,me,nb,kfp,kfn,xp,kfe,kfv,kfnu,entr$nucl,entr$neutrino,entr$electron,munu,entropy$total,entr$photon,ov,meth},
para=soltab[[5]];
set$couplings[para];
ov=OptionValue[add$photons];
meth=OptionValue[integration$method];
T=soltab[[3]];
nb=soltab[[4]];
sig=soltab[[1,1]];
w0=soltab[[1,2]];
r03=soltab[[1,3]];
mun=soltab[[1,4]];
mup=soltab[[1,5]];
me=0.511; (*electron mass in MeV, this should be added to para, not hardcoded*)
mue=If[NumericQ[soltab[[1,6]]],mue=soltab[[1,6]],If[mup>0,mun-mup,0]]; 
munu=If[NumericQ[soltab[[1,7]]],munu=soltab[[1,7]],0]; (*all solvers should return mu_e and mu_nu now so this should work*)
muv={mun,mup};
xp=proton$fraction$RMF[soltab];
kfp=(3*\[Pi]^2*xp*nb)^(1/3);
kfn=(3*\[Pi]^2*(1-xp)*nb)^(1/3);kfv={kfn,kfp};
kfe=(3*\[Pi]^2*n$electron[T,mue])^(1/3);
kfnu=(3*\[Pi]^2*n$neutrino[T,munu])^(1/3);
dsdT=If[T>0,Sum[-2/(2\[Pi])^3*4\[Pi]*Sum[NIntegrate[(E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]+sgn muv[[BI]])/T) k^2 (Log[1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))]-Log[1-1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))]) (sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]]))/((E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T)+E^((sgn muv[[BI]])/T))^2 T^2),{k,Max[0,kfv[[BI]]-boundmult*T],kfv[[BI]]+boundmult*T},Method->{meth, MaxPoints->10^5}],{BI,1,2}],{sgn,-1,+1,2}],0];


(*entr$nucl=If[T>0,Sum[-2/(2\[Pi])^3*4\[Pi]*Sum[NIntegrate[k^2((1-fd$bar[k,sig,sgn*w0,sgn*r03,sgn*muv[[BI]],BI,T])Log[1-fd$bar[k,sig,sgn*w0,sgn*r03,sgn*muv[[BI]],BI,T]]+fd$bar[k,sig,sgn*w0,sgn*r03,sgn*muv[[BI]],BI,T]*Log[fd$bar[k,sig,sgn*w0,sgn*r03,sgn*muv[[BI]],BI,T]]),{k,Max[0,kfv[[BI]]-boundmult*T],kfv[[BI]]+boundmult*T},Method->{meth, MaxPoints->10^7}],{BI,1,2}],{sgn,-1,+1,2}],0];

entr$electron=If[OptionValue[electrons],If[T>0,Sum[-2/(2\[Pi])^3*4\[Pi]*NIntegrate[k^2*(fd[Sqrt[k^2+me^2],sgn*mue,-T]*Log[fd[Sqrt[k^2+me^2],sgn*mue,-T]]+fd[Sqrt[k^2+me^2],sgn*mue,T]*Log[fd[Sqrt[k^2+me^2],sgn*mue,T]]),{k,Max[0,kfe-boundmult*T],kfe+boundmult*T}],{sgn,-1,+1,2}],0],0];
entr$neutrino=If[OptionValue[neutrinos],neutrino$entropy[T,munu,4000],0];
entr$photon=If[ov,photon$entropy[T],0];
If[OptionValue[electrons]==False&&mue!=0,Print["WARNING: electrons have finite chemical potential but electron flag = False"]];
If[OptionValue[neutrinos]==False&&munu!=0,Print["WARNING: neutrinos have finite chemical potential but neutrino flag = False"]];
entropy$total=entr$photon+entr$nucl+entr$electron+entr$neutrino;*)
Return[dsdT];
];


Options[d2s$dT2]={add$photons->False,integration$method->"LocalAdaptive",electrons->True,neutrinos->False};
d2s$dT2[soltab_,boundmult_Integer:40,OptionsPattern[]]:=Module[{T,dsdT,para,sig,w0,r03,mun,mup,muv,mue,meson$pressure,nucpress,me,nb,kfp,kfn,xp,kfe,kfv,kfnu,entr$nucl,entr$neutrino,entr$electron,munu,entropy$total,entr$photon,ov,meth},
para=soltab[[5]];
set$couplings[para];
ov=OptionValue[add$photons];
meth=OptionValue[integration$method];
T=soltab[[3]];
nb=soltab[[4]];
sig=soltab[[1,1]];
w0=soltab[[1,2]];
r03=soltab[[1,3]];
mun=soltab[[1,4]];
mup=soltab[[1,5]];
me=0.511; (*electron mass in MeV, this should be added to para, not hardcoded*)
mue=If[NumericQ[soltab[[1,6]]],mue=soltab[[1,6]],If[mup>0,mun-mup,0]]; 
munu=If[NumericQ[soltab[[1,7]]],munu=soltab[[1,7]],0]; (*all solvers should return mu_e and mu_nu now so this should work*)
muv={mun,mup};
xp=proton$fraction$RMF[soltab];
kfp=(3*\[Pi]^2*xp*nb)^(1/3);
kfn=(3*\[Pi]^2*(1-xp)*nb)^(1/3);kfv={kfn,kfp};
kfe=(3*\[Pi]^2*n$electron[T,mue])^(1/3);
kfnu=(3*\[Pi]^2*n$neutrino[T,munu])^(1/3);
dsdT=If[T>0,Sum[-2/(2\[Pi])^3*4\[Pi]*Sum[NIntegrate[(E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]+sgn muv[[BI]])/T) k^2 (sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]]) (-2 E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T) T Log[1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))]-2 E^((sgn muv[[BI]])/T) T Log[1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))]+2 E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T) T Log[1-1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))]+2 E^((sgn muv[[BI]])/T) T Log[1-1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))]+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T) sgn w0 gw[[BI]]+E^((sgn muv[[BI]])/T) sgn w0 gw[[BI]]+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T) sgn w0 Log[1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] gw[[BI]]-E^((sgn muv[[BI]])/T) sgn w0 Log[1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] gw[[BI]]-E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T) sgn w0 Log[1-1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] gw[[BI]]+E^((sgn muv[[BI]])/T) sgn w0 Log[1-1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] gw[[BI]]+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T) r03 sgn gr[[BI]] I3[[BI]]+E^((sgn muv[[BI]])/T) r03 sgn gr[[BI]] I3[[BI]]+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T) r03 sgn Log[1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] gr[[BI]] I3[[BI]]-E^((sgn muv[[BI]])/T) r03 sgn Log[1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] gr[[BI]] I3[[BI]]-E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T) r03 sgn Log[1-1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] gr[[BI]] I3[[BI]]+E^((sgn muv[[BI]])/T) r03 sgn Log[1-1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] gr[[BI]] I3[[BI]]+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T) Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]+E^((sgn muv[[BI]])/T) Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T) Log[1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-E^((sgn muv[[BI]])/T) Log[1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T) Log[1-1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]+E^((sgn muv[[BI]])/T) Log[1-1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T) sgn muv[[BI]]-E^((sgn muv[[BI]])/T) sgn muv[[BI]]-E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T) sgn Log[1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] muv[[BI]]+E^((sgn muv[[BI]])/T) sgn Log[1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] muv[[BI]]+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T) sgn Log[1-1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] muv[[BI]]-E^((sgn muv[[BI]])/T) sgn Log[1-1/(1+E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2]-sgn muv[[BI]])/T))] muv[[BI]]))/((E^((sgn w0 gw[[BI]]+r03 sgn gr[[BI]] I3[[BI]]+Sqrt[k^2+(-sig gs[[BI]]+mB[[BI]])^2])/T)+E^((sgn muv[[BI]])/T))^3 T^4),{k,Max[0,kfv[[BI]]-boundmult*T],kfv[[BI]]+boundmult*T},Method->{meth, MaxPoints->10^5}],{BI,1,2}],{sgn,-1,+1,2}],0];


(*entr$nucl=If[T>0,Sum[-2/(2\[Pi])^3*4\[Pi]*Sum[NIntegrate[k^2((1-fd$bar[k,sig,sgn*w0,sgn*r03,sgn*muv[[BI]],BI,T])Log[1-fd$bar[k,sig,sgn*w0,sgn*r03,sgn*muv[[BI]],BI,T]]+fd$bar[k,sig,sgn*w0,sgn*r03,sgn*muv[[BI]],BI,T]*Log[fd$bar[k,sig,sgn*w0,sgn*r03,sgn*muv[[BI]],BI,T]]),{k,Max[0,kfv[[BI]]-boundmult*T],kfv[[BI]]+boundmult*T},Method->{meth, MaxPoints->10^7}],{BI,1,2}],{sgn,-1,+1,2}],0];

entr$electron=If[OptionValue[electrons],If[T>0,Sum[-2/(2\[Pi])^3*4\[Pi]*NIntegrate[k^2*(fd[Sqrt[k^2+me^2],sgn*mue,-T]*Log[fd[Sqrt[k^2+me^2],sgn*mue,-T]]+fd[Sqrt[k^2+me^2],sgn*mue,T]*Log[fd[Sqrt[k^2+me^2],sgn*mue,T]]),{k,Max[0,kfe-boundmult*T],kfe+boundmult*T}],{sgn,-1,+1,2}],0],0];
entr$neutrino=If[OptionValue[neutrinos],neutrino$entropy[T,munu,4000],0];
entr$photon=If[ov,photon$entropy[T],0];
If[OptionValue[electrons]==False&&mue!=0,Print["WARNING: electrons have finite chemical potential but electron flag = False"]];
If[OptionValue[neutrinos]==False&&munu!=0,Print["WARNING: neutrinos have finite chemical potential but neutrino flag = False"]];
entropy$total=entr$photon+entr$nucl+entr$electron+entr$neutrino;*)
Return[dsdT];
];



End[];
EndPackage[];
