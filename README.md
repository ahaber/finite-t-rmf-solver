# finte-T-RMF-solver

 
This code is used to solve relativistic mean field models like IUF, SFHo, QMC-RMF1, QMC-RMF2, QMC-RMF3, and QMC-RMF4 as published in arxiv:2205.10283, and others. A collection of RMFs is provided in "EOSparamter.m" and can be extended by the user. The model parameters are defined in the manual "RMFsolver_manual.pdf".
 
This module can solve RMFs at finite temperature and arbitrary proton fractions (in and out of chemical equilibrium, pure neutron matter, symmetric nuclear matter, ...). Several functions to compute thermodynamic quantities (pressure, energy density, binding energy, entropy density, equation of state, baryon density, proton fraction, direct Urca threshold,...) for infinite, homogeneous nuclear matter are provided.
 
"RMFsolver_finiteT_example.nb" is an example notebook that shows how the module can be used to compute the finite T pressure of the IUF model.
 
The modules "Constants.m" and "RMFsolver.m" are required.
 
Questions? contact: ahaber@physics.wustl.edu

